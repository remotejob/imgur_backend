FROM scratch
EXPOSE 8080
ADD ca-certificates.crt /etc/ssl/certs/
COPY server /
CMD ["/server"]