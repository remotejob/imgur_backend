package main

import (
	"log"
	"net/http"
	"net/url"
	"os"

	"strings"

	"github.com/bahlo/goat"
	"github.com/dghubble/sling"
	"github.com/gorilla/handlers"
)

const imgurAPI = "https://api.imgur.com/3/"

var imgurClientID = os.Getenv("IMGURCLIENTID")

var base = sling.New().Base(imgurAPI).Add("Authorization", "Client-ID "+imgurClientID)

type searchBasicResponse struct {
	Data    []dataSearchBasicResponse `json:"data"`
	Success bool                      `json:"success"`
	Status  int                       `json:"status"`
}
type jsonResponse struct {
	ResponseType string         `json:"response_type,omitempty"`
	Text         string         `json:"text,omitempty"`
	Attachments  []attachements `json:"attachments"`
}

type attachements struct {
	Fallback string `json:"fallback"`
	ImageURL string `json:"image_url,omitempty"`
}

type images struct {
	Images []dataSearchBasicResponse
}

type imageBasicResponse struct {
	Data    dataImageBasicResponse `json:"data"`
	Success bool                   `json:"success"`
	Status  int                    `json:"status"`
}

type dataSearchBasicResponse struct {
	ID           string `json:"id"`
	Title        string `json:"title"`
	Description  string `json:"description"`
	Datetime     int    `json:"datetime"`
	Cover        string `json:"cover"`
	CoverWidth   int    `json:"cover_width"`
	CoverHeight  int    `json:"cover_height"`
	AccountURL   string `json:"account_url"`
	AccountID    int    `json:"account_id"`
	Privacy      string `json:"privacy"`
	Layout       string `json:"layout"`
	Views        int    `json:"views"`
	Link         string `json:"link"`
	Ups          int    `json:"ups"`
	Downs        int    `json:"downs"`
	Points       int    `json:"points"`
	Score        int    `json:"score"`
	IsAlbum      bool   `json:"is_album"`
	Vote         string `json:"vote"`
	Favorite     bool   `json:"favorite"`
	Nsfw         bool   `json:"nsfw"`
	Section      string `json:"section"`
	CommentCount int    `json:"comment_count"`
	Topic        string `json:"topic"`
	TopicID      int    `json:"topic_id"`
	ImagesCount  int    `json:"images_count"`
	InGallery    bool   `json:"in_gallery"`
	IsAd         bool   `json:"is_ad"`
}
type dataImageBasicResponse struct {
	ID           string `json:"id"`
	Title        string `json:"title"`
	Description  string `json:"description"`
	Datetime     int    `json:"datetime"`
	Type         string `json:"type"`
	Animated     bool   `json:"animated"`
	Width        int    `json:"width"`
	Hight        int    `json:"height"`
	Size         int    `json:"size"`
	Views        int    `json:"views"`
	Bandwidth    int    `json:"bandwidth"`
	Link         string `json:"link"`
	Gifv         string `json:"gifv"`
	Mp4          string `json:"mp4"`
	Mp4Size      int    `json:"mp4_size"`
	Looping      bool   `json:"looping"`
	Vote         string `json:"vote"`
	Favorite     bool   `json:"favorite"`
	Nfsw         bool   `json:"nsfw"`
	CommentCount int    `json:"comment_count"`
	Topic        string `json:"topic"`
	TopicID      int    `json:"topic_id"`
	Section      string `json:"section"`
	AccountURL   string `json:"account_url"`
	AccountID    int    `json:"account_id"`
	Ups          int    `json:"ups"`
	Downs        int    `json:"downs"`
	Points       int    `json:"points"`
	Score        int    `json:"score"`
	IsAlbum      bool   `json:"is_album"`
}

func filterImages(images []dataSearchBasicResponse) []dataSearchBasicResponse {
	nsfwFilter := false
	isAlbumFilter := false

	filteredImages := []dataSearchBasicResponse{}

	for _, e := range images {
		if e.Nsfw == nsfwFilter {
			if e.IsAlbum == isAlbumFilter {

				u, err := url.Parse(e.Link)
				if err != nil {
					panic(err)
				}

				splitPath := strings.Split(u.Path, ".")

				if splitPath[1] != "gif" {

					filteredImages = append(filteredImages, e)
				}
			}
		}
	}
	return filteredImages
}
func getFrontPageImgurImage() []dataSearchBasicResponse {

	log.Println("getFrontPageImgurImage Start")
	galleryFP := base.New().Get("gallery/").Path("hot/viral/")
	// galleryFP := base.New().Get("gallery/")
	fpResp := new(searchBasicResponse)
	_, searchErr := galleryFP.ReceiveSuccess(fpResp)
	if searchErr != nil {
		log.Fatal(searchErr)
	}

	images := filterImages(fpResp.Data)

	return images
}
func slackgurHandler(w http.ResponseWriter, r *http.Request, p goat.Params) {

	imgtype := r.Header.Get("X-Thumbnails-Header")
	imgs := getFrontPageImgurImage()

	log.Println("imgtype", imgtype)

	if imgtype != "" {

		for i, img := range imgs {

			u, err := url.Parse(img.Link)
			if err != nil {
				panic(err)
			}
			path := u.Path

			splitPath := strings.Split(path, ".")
			newpath := splitPath[0] + imgtype + "." + splitPath[1]
			u.Path = newpath

			log.Println(u.String())
			imgs[i].Link = u.String()

		}

	}

	goat.WriteJSON(w, images{imgs})

}
func corsMiddleware(h http.Handler) http.Handler {

	corsAllowedOrigins := handlers.AllowedOrigins([]string{"*"})
	corsAllowedHeaders := handlers.AllowedHeaders([]string{"X-Thumbnails-Header"})
	return handlers.CORS(corsAllowedOrigins, corsAllowedHeaders)(h)
}
func main() {
	r := goat.New()
	r.Get("/", "slackgur", slackgurHandler)
	// r.Post("/", "slackgur", slackgurHandler)
	r.Use(corsMiddleware)
	r.Run(":8080")
}
